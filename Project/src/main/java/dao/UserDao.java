package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.User;

public class UserDao {
	//ログインIDとパスワードに紐づくユーザ情報を返す
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			//DB接続
			conn = DBManager.getConnection();

			//SELECT文
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			//キーに基づくレコードは1件のみなので、rs.next()は１回だけ行う
			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
			boolean isAdmin = rs.getBoolean("is_admin");
			Timestamp createDate = rs.getTimestamp("create_date");
			Timestamp updateDate = rs.getTimestamp("update_date");
			return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
	}

	//ユーザー情報習得(UserList)

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();
		try {
			conn = DBManager.getConnection();

			//SELEST文の準備と結果の取得
			String sql = "SELECT * FROM user";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			//レコードの内容をUserインスタンスに設定しAllayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				boolean isAdmin = rs.getBoolean("is_admin");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//ユーザー情報登録(UserAdd)
	public User insert(String loginId, String name, String password) {
		//DB接続
		Connection con = null;
		try {
			con = DBManager.getConnection();
			String insertSQL = "INSERT INTO user (login_id,name,birth_date,password,create_date,update_date) VALUE (?,?,?,?,?,?);";
			
			//実行
			PreparedStatement stmt = con.prepareStatement(insertSQL);
			//SQLに値を登録
			stmt.setString(1, loginId);
			stmt.setString(2, name);
			stmt.setString(3, birthday);
			stmt.setString(4, password);
			stmt.setString(5, birthday);
			stmt.setString(6, birthday);
			
			
			

			//登録SQL実行
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			 return;
	    } finally {
	      // データベース切断
	      if (con != null) {
	        try {
	          con.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	          return;
	        }
	      }
	    }
	}

	//詳細表示(UserDetail)
	public static  List<User> findById(int selectIdInt) {
		Connection conn = null;
		List<User> userListEach = new ArrayList<User>();
		try {
			//DB接続
			conn = DBManager.getConnection();

			//SELECT文
			String sql = "SELECT * FROM user WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, selectIdInt);
			ResultSet rs = pStmt.executeQuery();

			//キーに基づくレコードは1件のみなので、rs.next()は１回だけ行う
			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
			boolean isAdmin = rs.getBoolean("is_admin");
			Timestamp createDate = rs.getTimestamp("create_date");
			Timestamp updateDate = rs.getTimestamp("update_date");
			User userEach = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);
			userListEach.add(userEach);
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userListEach;
	}


	
	//更新(UserUpdate)
	public User update(String name, String password, String birthday) {
		//DB接続
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			String updateSQL = "UPDATE user SET name = ?,birth_date = ?, password=? WHERE id =?";
			PreparedStatement pstmt = con.prepareStatement(updateSQL);
			//SQLに値を登録
			pstmt.setString(1, name);
			pstmt.setString(2, birthday);
			pstmt.setString(3, password);
			pstmt.setInt(4, updateId);

			//登録SQL実行
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		      // データベース切断
		      if (con != null) {
		        try {
		          con.close();
		        } catch (SQLException e) {
		          e.printStackTrace();
		          return null;
	}
		      }
		
		}
		return null;
	}

	
	//削除(DeleteException)
	public User delete(int deleteIdint) {
		Connection con = null;
		try {
			con = DBManager.getConnection();
			String deleteSQL = "DELETE FROM user WHERE id =?;";
			//DELETE文実行
			PreparedStatement stmt = con.prepareStatement(deleteSQL);;
			//stmt = con.prepareStatement(insertSQL);
			//SQLに値を登録
			stmt.setInt(1, deleteIdint);
			//登録SQL実行
			stmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return;
	
	}
		
		
			
	

//検索
	public User searchForId() {
		//SELECT文の
		String sqlSearchId = "SELECT * FROM user WHERE id =?;";
		return null;
		
	}
	
	public User searchForName() {
		return null;
		}
	
	public User searchForDate() {
		return null;
		
	}
	}
 

