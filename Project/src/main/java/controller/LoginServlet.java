package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        /**@see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
           }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		//フォワード
	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
	dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//リクエストパラメーターの文字コード指定
		request.setCharacterEncoding("UTF-8");
		//リクエストパラメーターの入力項目を因数にして、Daoメソッドを実行
		String loginId = request.getParameter("loginid");
		String password = request.getParameter("password");
		
		//リクエストパラメーターの入力項目を因数に渡してDaoメソッドを実行
		UserDao userDao = new UserDao();
		User user = userDao.findByLoginInfo(loginId,password);
		
		//①テーブルに該当のデータが、みつからなかった時
		if(user == null) {
		request.setAttribute("errMsg","ログインに失敗しました")	;
		request.setAttribute("loginId", loginId);
		//ログインjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
		dispatcher.forward(request, response);
		return;
	}
		//②ログインセッションがある場合、ユーザー一覧画面にリダイレクトさせる
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);
		response.sendRedirect("UserListServlet");
	}
}
	  
	