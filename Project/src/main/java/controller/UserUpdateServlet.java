package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//
		String selectId = request.getParameter("id");
		int selectIdInt = Integer.valueOf(selectId);
		//
		//UserDao userDao = new UserDao();
		List<User> userListEach = UserDao.findById(selectIdInt);
		//ユーザー情報をセット
		request.setAttribute("userListEach", userListEach);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
 			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		// パラメータから値を取得
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("password-confirm");
		String name = request.getParameter("user-name");
		String birthday = request.getParameter("user-name");
		if(password !=  passwordConfirm) {
			request.setAttribute("errMsg","登録に失敗しました")	;
		}
		//ユーザー情報登録
		UserDao.update(name, password,birthday);
		//ユーザーサーブレットへリダイレクト
		response.sendRedirect("UserListServlet");
		
	
	}

}
