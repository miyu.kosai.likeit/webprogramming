package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		//ログインチェック
		//セッションスコープからuserInfoをキーにしてログインしているユーザー情報を取得
		HttpSession session = request.getSession();
		User uInfo =(User)session.getAttribute("userInfo");
		
		//ユーザー情報が取得できない場合LoginServletへリダイレクト
		if(uInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
			}
		//ユーザー情報習得できたら、userAdd.jspに遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
	     dispatcher.forward(request, response);
	     return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");
		// パラメータから値を取得
		String loginId = request.getParameter("user-id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("password-confirm");
		String name = request.getParameter("user-name");
		String birthday = request.getParameter("user-name");
		if(password !=  passwordConfirm) {
			request.setAttribute("errMsg","登録に失敗しました")	;
		request.setAttribute("loginId", loginId);
		}
		//ユーザー情報登録
		UserDao.insert(loginId, name, password,birthday);
		//ユーザーサーブレットへリダイレクト
		response.sendRedirect("UserListServlet");
		
	}
	

	
}
