package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserDeleteExcutionServlet
 */
@WebServlet("/UserDeleteExcutionServlet")
public class UserDeleteExcutionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteExcutionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		// パラメータから値を取得
		String deleteId = request.getParameter("id");
		int deleteIdint = Integer.valueOf(deleteId);
				/*if(password !=  passwordConfirm) {
			System.out.println("パスワードが一致しません");
		}else {*/
		//ユーザー情報登録
		//User UserDao = new User();
		UserDao.delete(deleteIdint);
		//ユーザーサーブレットへリダイレクト
		response.sendRedirect("UserDeleteServlet");
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
