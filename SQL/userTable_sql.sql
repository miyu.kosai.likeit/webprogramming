﻿CREATE DATABASE usermanagement CHARACTER SET utf8;
USE usermanagement;
CREATE TABLE user(id SERIAL PRIMARY KEY AUTO_INCREMENT NOT NULL,login_id VARCHAR(255) UNIQUE NOT NULL,
name VARCHAR(255) NOT NULL,birth_date DATE NOT NULL,password VARCHAR(255) NOT NULL,
is_admin boolean NOT NULL default false,create_date DATETIME NOT NULL,update_date DATETIME NOT NULL );
INSERT INTO user (login_id,name,birth_date,password,is_admin,create_date,update_date) 
VALUES('admin','管理者','2020-02-02','password',true,now(),now());

USE usermanagement;
INSERT INTO user(login_id,name,birth_date,password,is_admin,create_date,update_date)VALUES('user01','一般1','2001-12-31','password',false,now(),now())
INSERT INTO user(login_id,name,birth_date,password,is_admin,create_date,update_date)VALUES('user02','一般2','2001-01-01','password',false,now(),now())
INSERT INTO user(login_id,name,birth_date,password,is_admin,create_date,update_date)VALUES('user03','一般3','2002-02-02','password',false,now(),now());

USE usermanagement;
SELECT * FROM user WHERE login_id ='admin'and password = 'password';
SELECT * FROM user WHERE login_id =? and password =?;

